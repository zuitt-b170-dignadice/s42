document.querySelector("#txt-first-name");
/*
document - refers to the whole webpage
querySelector - used to select a specific element(object) as long as it is inside the html tag (HTML element)
						- takes a string input that is formatted like CSS selector
						-can select elements regardless if the string is an id, a class or a tag; as long as the element is existing in the webpage

*/

/*
document.querySelector is similar to these three:

document.getElementById("txt-first-name");
document.getElementByClass("txt-inputs");
document.getElementByTagName("input");
*/

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const txtFullName = document.querySelector("#span-full-name");




const updateFullname = () => {
    let firstName = txtFirstName.value
    let lastName = txtLastName.value

    txtFullName.innerHTML = `${firstName} ${lastName}`
}


txtFirstName.addEventListener("keyup", updateFullname)
txtLastName.addEventListener("keyup", updateFullname)



// full.addEventListener("keyup", (event) =>{
// 	txtFullName.innerHTML = full.value;

// })