document.querySelector("txt-first-name")

/* 

document - refers to the whole webpage

querySelector = used to select a specific element(object) as loing as it is inside the html tag (HTML elememnt)

- takes a string input that is formatted like CSS selector 
- can select elements regardless if the string is an id, class or a tag

as long as the element is existing in the webpage

*/

// document.querySelector is similar to these three : 

// document.getElementById("txt-first-name")
// document.getElementById("txt-last-name")
// document.getElementsByClassName("txt-inputs")
// document.getElementsByTagName("input")

const txtFirstName = document.querySelector("#txt-first-name")
const txtFull1Name = document.querySelector("#span-full-name")


txtFirstName.addEventListener("keyup", (event) => {
    txtFull1Name.innerHTML = txtFirstName.value
})

/* 
    event - actions that the user is doing in our webpage (scroll, click, hover, keypress/type)

    addEventListener 0- a function that lets the webpage to listen to the evvents performed by the user 

    takes two arguments,
    - string - the event on which the HTML element will listen
    - function - executed by the ement once the event (first argument) is triggered
    - 


*/

// makes the element listen to multiple events
txtFirstName.addEventListener("keyup", (event) => {
    // trying to log othe codes, instead of its value  once the vent is triggered
    console.log(event.target)
    // trying to log the value of the element, once the event is triggered
    console.log(event.target.value)
})

/* 
make another "keyup" event where in the span el;ement will record the last name in the form

send the output in the batch google chat
*/

const txtLastName = document.querySelector("#txt-last-name")
txtLastName.addEventListener("keyup", (event) => {
    txtFull2Name.innerHTML = txtLastName.value
})